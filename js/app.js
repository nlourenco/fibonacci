function init() {
    var number = prompt("Please enter a number from 0 to 10 to Fibonate!");
    
    var container = document.getElementById("container");
    
    if(number <= 10 && number != null && !isNaN(number)){
        container.innerHTML = FiboThis(number);
    }else{
        container.innerHTML = "Your number was either above 10 or not a whole number."
    }
}
function FiboThis(n) {
    var aux = parseInt(n);
    if (aux == 0) {
        return 0;
    }
    if (aux == 1) {
        return 1;
    }
    else {
        return FiboThis(n - 1) + FiboThis(n - 2);
    }
}

init();